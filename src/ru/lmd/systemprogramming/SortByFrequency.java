package ru.lmd.systemprogramming;

import java.util.*;

/**
 * Класс для сортировки метода по частоте
 *
 * @author M. Lebedev 17IT18
 */
public class SortByFrequency {

    public static void main(String[] args) {
        ArrayList<Integer> resultOfArrayList;
        resultOfArrayList = sort();
        System.out.println(resultOfArrayList);
    }

    /**
     * Метод производит сортировку по частоте и выводит реультат на консоль
     *
     * @return отсортированный массив
     */
    private static ArrayList<Integer> sort() {
        LinkedHashMap<Integer, Integer> hashMap;
        hashMap = SortByFrequency.input();
        ArrayList<Integer> arrayList = new ArrayList<>();
        List<Integer> mapValue = new ArrayList<>(hashMap.values());
        List<Integer> mapKeys = new ArrayList<>(hashMap.keySet());

        while (true) {
            int maxNumber = Collections.max(mapValue);
            int index = mapValue.indexOf(maxNumber);
            for (int p = 0; p < maxNumber; p++) {
                int countZero = 0;
                arrayList.add(mapKeys.get(index));
                mapValue.set(index, 0);
                for (int h = 0; h < mapValue.size(); h++) {
                    if (mapValue.get(h) == 0) {
                        countZero++;
                        if (countZero == mapValue.size()) {
                            return arrayList;
                        }
                    }

                }
            }
        }
    }

    /**
     * Метод производит ввод данных
     *
     * @return массив, который ввели
     */
    private static LinkedHashMap input() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int longOfArray = sc.nextInt();
        System.out.println("Введите массив");

        LinkedHashMap<Integer, Integer> hashMap = new LinkedHashMap<>();

        for (int i = 0; i < longOfArray; i++) {
            int number = sc.nextInt();
            Integer frequency = hashMap.get(number);
            hashMap.put(number, frequency == null ? 1 : frequency + 1);
        }
        return hashMap;
    }
}
